const calc = require('./calculadora');
const prompt = require('prompt-sync')();

let valor1 = prompt("Informe o primeiro valor:");
let valor2 = prompt("Informe o segundo valor:");
console.log("Total: " + calc.somar(valor1, valor2));



/* 
console.log("Soma: " + calc.somar(10,5));
console.log("Subtração: " + calc.subtrair(20, 7));
console.log("Multiplicação: " + calc.multiplicar(5, 10));
console.log("Dividir: " + calc.dividir(100, 20));

 */

//função tradicional
/* function somar(a, b) {
    return a + b;
} */

//função seta (arrow function)
/* let subtrair = (a, b) => a - b;

let valor = somar(10, 5);
console.log(valor);

let result = subtrair(20, 10);
console.log(result);
 */



/* 
let notas = [4,7,3];

for(nota of notas){
    console.log(nota);
} 
*/


//console.log(notas);
//console.log(notas[1]);
/* 
for (i in notas){
    console.log(notas[i]);
}
 */

/* 
let contador = 0;
while(contador < 3){
    console.log("Nota " + (contador + 1) + ": " + notas[contador]);
    contador++;
}
*/
/* 
for(let i = 0; i < 3; i++){
    console.log("Nota " + (i + 1) + ": " + notas[i]);
}
 */

//Operadores e estruturas de decisão
/* 
let nota1 = 10;
let nota2 = 6;

let notaFinal = (nota1 + nota2)  / 2;

if (notaFinal >= 6){
    console.log("Aprovado!");
} else {
    console.log("Reprovado!");
}

console.log(notaFinal);
 */

//variáveis  var, let, const

/* 
let numero = 100;
console.log(numero);
console.log(typeof numero);

let texto = "Pedro";
console.log(texto);
console.log(typeof texto);

let aprovado = true;
console.log(aprovado);
console.log(typeof aprovado); 
*/