// function somar(a, b) {
//     return a + b;
// }

const somar = (a, b) => parseFloat(a) + parseFloat(b);
const subtrair = (a, b) => a - b;
const multiplicar = (a, b) => a * b;
const dividir = (a, b) => a / b;

module.exports = {
    somar,
    subtrair,
    multiplicar,
    dividir
};